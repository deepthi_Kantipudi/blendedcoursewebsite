import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from'@angular/router';
import { FormsModule }    from '@angular/forms';
import { HttpModule }    from '@angular/http';
import { EmbedVideo } from 'ngx-embed-video';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { HomeComponent } from './home/home.component';
import { ModuleComponent } from './module/module.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule, MatButtonModule, MatSidenavModule, MatIconModule, MatListModule } from '@angular/material';
import { SyllabusComponent } from './syllabus/syllabus.component';
import { CalenderComponent } from './calender/calender.component';
import { AnnouncementComponent } from './announcement/announcement.component';
const appRoutes: Routes = [
  { path: 'home', component :HomeComponent},
  { path: 'module', component :ModuleComponent},
  { path: 'syllabus', component :SyllabusComponent},
  { path: 'calender', component :CalenderComponent},
  { path: 'announcement', component :AnnouncementComponent},
  
];
@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    HomeComponent,
    ModuleComponent,
    SyllabusComponent,
    CalenderComponent,
    AnnouncementComponent,
    
    
  
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    RouterModule.forRoot(appRoutes),
    FormsModule,
    BrowserAnimationsModule,
    EmbedVideo.forRoot(),
    HttpModule
    
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { 
  
}
