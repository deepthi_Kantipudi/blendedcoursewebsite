import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HeaderComponent } from './header/header.component';
import { HomeComponent } from './home/home.component';
import { ModuleComponent } from './module/module.component';
import { CalenderComponent } from './calender/calender.component';
// import { SelectivePreloadingStrategyService } from './selective-preloading-strategy.service';

// const routes: Routes = [];

// const appRoutes: Routes = [
//   {
//     path: 'module',
//     component: ModuleComponent,
    
//   }]
@NgModule({
  imports: [
    // RouterModule.forRoot(
    //   appRoutes,
    //   {
    //     enableTracing: false, // <-- debugging purposes only
    //     // preloadingStrategy: SelectivePreloadingStrategyService,
    //   }
    // )
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
