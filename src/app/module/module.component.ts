import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-module',
  templateUrl: './module.component.html',
  styleUrls: ['./module.component.css']
})
export class ModuleComponent implements OnInit {
  show1:boolean = true;
  show2:boolean = false;
  show3:boolean = false;
  show4:boolean = false;
  show5:boolean = false;
  show6:boolean = false;
  show7:boolean = false;

 constructor() {

}

  ngOnInit() {
  }
 showM1(){
   this.show1=true;
   this.show2=false;
   this.show3=false;
   this.show4 = false;
   this.show5 = false;
   this.show6 = false;
   this.show7 = false;
 }
 showM2(){
  this.show1=false;
  this.show2=true;
  this.show3=false;
  this.show4 = false;
   this.show5 = false;
   this.show6 = false;
   this.show7 = false;
}
showM3(){
  this.show1=false;
  this.show2=false;
  this.show3=true;
  this.show4 = false;
   this.show5 = false;
   this.show6 = false;
   this.show7 = false;
}
showM4(){
  this.show1=false;
  this.show2=false;
  this.show3=false;
  this.show4 = true;
   this.show5 = false;
   this.show6 = false;
   this.show7 = false;
}
showM5(){
  this.show1=false;
  this.show2=false;
  this.show3=false;
  this.show4 = false;
   this.show5 = true;
   this.show6 = false;
   this.show7 = false;
}
showM6(){
  this.show1=false;
  this.show2=false;
  this.show3=false;
  this.show4 = false;
   this.show5 = false;
   this.show6 = true;
   this.show7 = false;
}
showM7(){
  this.show1=false;
  this.show2=false;
  this.show3=false;
  this.show4 = false;
   this.show5 = false;
   this.show6 = false;
   this.show7 = true;
 }
}
